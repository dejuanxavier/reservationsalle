/*
Ecris par Olivier Dupré
*/
CREATE DATABASE ReservationSalle;
USE ReservationSalle;

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, EXECUTE, 
CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, 
EVENT, TRIGGER ON *.* TO 'AdminResa'@'%' IDENTIFIED BY PASSWORD '*BD902B462E660D06D0D2CBC2BC489BEA66E171FC';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, INDEX, ALTER, CREATE TEMPORARY TABLES, 
EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER 
ON *.* TO 'AdminResa'@'%' IDENTIFIED BY PASSWORD 'R3serv@tion'; 

CREATE TABLE salles (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    numero INT,
    etage INT,
    secteur VARCHAR(100),
    batiment VARCHAR(100),
    nbPlaces INT,
    projecteur BOOLEAN,
    tableau BOOLEAN,
    tele BOOLEAN
);
-- Pour rajouter "tele" dans PHPMyAdmin ALTER TABLE salles ADD tele BOOLEAN;-------

CREATE TABLE reservations (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    salle_id BIGINT,
    user_id BIGINT,
    dateJour DATE,
    heureDepart TIME,
    heureFin TIME,
    message VARCHAR(255),
    recurrence_id BIGINT
    -- Pour rajouter "recurrence_id" dans PHPMyAdmin ALTER TABLE reservations ADD recurrence_id BIGINT;-------
);

CREATE TABLE users (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(255),
    prenom VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255),
    admin BOOLEAN,
    actif BOOLEAN,
    token VARCHAR(255),
    code VARCHAR(255)
);
-- Pour rajouter "code" dans PHPMyAdmin ALTER TABLE users ADD code VARCHAR(255);-------


-- A copier dans vos MySQL --

CREATE TABLE reservations_reccurentes (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    dateDebut DATE,
    dateFin DATE,
    type INT,
    user_id BIGINT
);

INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `password`, `admin`, `actif`, `token`, `code`) VALUES
(1, 'Admin', 'Admin', 'admin@realise.ch', '$2y$10$vBzzgNfx.xhvK0Bye50k4uP/oCLPvfbS1nUJeI45TFAUy3Qqd/UMm', 1, 1, '5d89ffb3b7579', 'AA');


----------------------------creation users-------------------------------------
INSERT INTO `users` SET `nom` = 'DuprÃ©', `prenom` = 'Olivier', `email` = 'olivier.dupre@realise.ch',
 `password` = 'Digital2019'

INSERT INTO `users` SET `nom` = 'Omar', `prenom` = 'Abdikarim', `email` = 'abdikarim.omar@realise.ch',
 `password` = 'Digital2019'

INSERT INTO `users` SET `nom` = 'Moran', `prenom` = 'Hilda', `email` = 'hilda.moran@realise.ch',
 `password` = 'Digital2019'

INSERT INTO `users` SET `nom` = 'Jedi', `prenom` = 'Steve', `email` = 'steve.jedi@realise.ch',
 `password` = 'Digital2019'

INSERT INTO `users` SET `nom` = 'Enchort', `prenom` = 'Tamer', `email` = 'tamer.enchort@realise.ch',
 `password` = 'Digital2019'

 ----------------------------creation salle-------------------------------------

INSERT INTO `salles` SET `numero` = '306', `etage` = '3', `secteur` = 'IEmploi',
 `batiment` = 'Marziano', `nbPlaces` = '8', `projecteur` = '0', `tableau` = '1'

INSERT INTO `salles` SET `numero` = '405', `etage` = '4', `secteur` = 'IEmploi',
 `batiment` = 'Marziano', `nbPlaces` = '8', `projecteur` = '0', `tableau` = '0'


INSERT INTO `salles` SET `id` = '4', `numero` = '305', `etage` = '3', `secteur` = 'IEmploi',
 `batiment` = 'Marziano', `nbPlaces` = '8', `projecteur` = '1', `tableau` = '0'

INSERT INTO `salles` SET `id` = '8', `numero` = '406', `etage` = '4', `secteur` = 'IEmploi',
 `batiment` = 'Marziano', `nbPlaces` = '9', `projecteur` = '0', `tableau` = '1' 